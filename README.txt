## PhD useful links
# Handbook for doctoral studies, Procedure for licentiate Thesis
https://www.chalmers.se/insidan/EN/education-research/doctoral-student/handbook-for-doctoral8636

https://student.portal.chalmers.se/doctoralportal/Pages/Doctoralportal.aspx
https://www.chalmers.se/insidan/SV/utbildning-och-forskning/forskarutbildning
https://www.chalmers.se/insidan/EN/

# Get list of PhD students' email address:
Look at PhD-Emaillist.txt

# Template for answering interested student (in PhD level).
Look at templateEmail.txt

# PRIMULA: expense report , vacation
https://personal.portal.chalmers.se/chalmers/

# Book trip via egencia online: 
http://go.egencia.com/R00y00fT0X003000FrYu6S0
The first attempt for registration usually fails :), don't get disappointed, try again
http://chalmers.egencia.se/app?service=external&page=Login&mode=form&market=SE
Email address to ask questions or order via email: Team3.got@viaegencia.com
# List of passed course and certificate
https://student.portal.chalmers.se/en/chalmersstudies/Pages/services.aspx

# Course schedules, room info
https://se.timeedit.net/web/chalmers/db1/public/#

## LRS , gym expense
http://plus.portal.chalmers.se/
https://www.benify.se/fps/welcomeCustomer.do?nav.id=520

## Student union
medlem.chs.chalmers.se (student union card)
Music room (& other rooms) in student union building access code : 112233
# Discount (Apple, Swebus, Comviq, Tele2, SF Bio, Adlibris, ...)
https://mecenat.com/se/

## Change CID, NOMAD password
 http://cdks.chalmers.se
## SSH servers:
remote12.chalmers.se
remote2.student.chalmers.se
remote11.chalmers.se

If you do not want to reenter your password all the time, add
the following two lines to the corresponding entries in $HOME/.ssh/config:

     GSSAPIAuthentication yes
     GSSAPIDelegateCredentials yes

After that, having a valid kerberos ticket will suffice to log in.

## Windows servers
rdesktop remote.cda.chalmers.se
login name: NET\CID or CID@net.chalmers.se

# Home dire
sftp://CID@remote12.chalmers.se/chalmers/users/CID
# Courswe directory
/chalmers/groups/edu2009/www/www.cse.chalmers.se/year/2015/course/TDA352_Cryptography

Windows home directoory:
file00.chalmers.se
Linux home directory as windows share:
sol.ita.chalmers.se
More info:
https://student.portal.chalmers.se/en/contactservice/ITServices/self-administered/linux/file-storage/Sidor/Mount-home-directory.aspx
## Chalmers provide free VPN, SVN and mysql & oracle databases 
https://student.portal.chalmers.se/en/contactservice/ITServices/self-administered/linux/Sidor/default.aspx

# VPN, SSH to work remotely and still have access to restricted articles and libraries
https://student.portal.chalmers.se/en/contactservice/ITServices/self-administered/Sidor/default.aspx

A more lightweight solution is to use sshuttle (https://github.com/apenwarr/sshuttle).
Running the following command will automatically tunnel all TCP connections and
DNS requests through the chalmers network:

sshuttle -r remote11.chalmers.se 0/0

## Email clients

# SMTP
smtp.chalmers.se : 587 
username : CID
Auth method: NTLM
Connection: STARTTLS

# IMAP
imap.chalmers.se : 993
username : CID
Auth method: Normal password
Connection: SSL/TLS
// Note : Thunderbird test doesn't work correctly.

Fix "Reply" sends an email to myself :
    Right-click on you account name
    Choose Settings...
    Click on Manage Identities...
    Add an Identity using the e-mail address for which Reply to All did not work correctly.

You might also have to set the config option mailnews.reply_to_self_check_all_ident to true

# Thunderbird/Outlook integration
Tools -> addon -> "Lightning" extension for outlook integration
                  "Send Later" extension.

## Free services
https://sv.sharelatex.com/
http://bitbucket.com
http://github.com
http://box.com
http://prezi.com


# Web space
/chalmers/users/CID/www/www.cse.chalmers.se
http://www.cse.chalmers.se/~CID/

## Free softwares
Server:
smb://syros.ce.chalmers.se
\\syros.ce.chalmers.se
Domain: NET
ID: Normal one
Password: Normal one                  

# Microsoft tools : Different version of Windows (even ancient DOS), SQL server and visual studio and ...
You need to specify your university (Chalmers) here
https://onthehub.com/microsoftimagine

## Printer

# web interface:
https://print.chalmers.se/auth/uploadme.cgi

# ubuntu 
Drivers
http://print.chalmers.se/drivers/

system-config-printer

smb://print.chalmers.se/cse-ed-5473-laser1
add PDD file (driver)

It fails to authenticate first, but you can set the password here when you look at printer queue :
username: NET/CID
password: cid password

# Printing via ipp/kerberos:
Copy krb5.conf from repository to /etc/krb5.conf and
run:

test -d $HOME/.cups || mkdir $HOME/.cups
cat <<EOF >$HOME/.cups/client.conf
ServerName print.chalmers.se
GSSServiceName HTTP
EOF

Printing should work whenever you have a valid kerberos ticket that
can be obtained by running `kinit $YOUR_CID`

# eduroam
There is installer here (even for Linux) https://cat.eduroam.org/ that embedded the eduroam certificate.
ID: CID
Anonymous ID: empty
Password: CID password
Authentication: MSCHAPv2
EAP type: PEAP
Eduroam certificate is in eduroam.pem

--
Hamid Ebadi
